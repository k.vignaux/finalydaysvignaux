package com.example.finalydaysvignaux.helpers

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.example.finalydaysvignaux.R
import com.example.finalydaysvignaux.models.Orders

class OrdersAdapter(private val ordersList: List<Orders>) :RecyclerView.Adapter<OrdersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view  = LayoutInflater.from(parent.context).inflate(R.layout.orders_item,parent,false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {

        return ordersList.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("Response", "List Count :${ordersList.size} ")


        return holder.bind(ordersList[position])

    }
    class ViewHolder(itemView : View) :RecyclerView.ViewHolder(itemView) {


        var pic = itemView.findViewById<ImageView>(R.id.OrdersFlag)
        var data = itemView.findViewById<TextView>(R.id.OrdersData)
        fun bind(orders: Orders) {
            data.text   ="Orders  :${orders.`data`}"

        }

    }
}