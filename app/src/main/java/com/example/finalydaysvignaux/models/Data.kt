package com.example.finalydaysvignaux.models

data class Data(
    val commands: List<Command>
)