package com.example.finalydaysvignaux.services

import com.example.finalydaysvignaux.models.Orders
import com.example.finalydaysvignaux.models.OrdersList
import retrofit2.Call
import retrofit2.http.GET

interface OrdersService {

    @GET("orders")
    fun getAffectedOrdersList () : Call<List<Orders>>
}