package com.example.finalydaysvignaux.activities

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalydaysvignaux.R
import com.example.finalydaysvignaux.helpers.OrdersAdapter
import com.example.finalydaysvignaux.models.Orders
import com.example.finalydaysvignaux.models.OrdersList
import com.example.finalydaysvignaux.services.OrdersService
import com.example.finalydaysvignaux.services.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadOrders()
    }

    private fun loadOrders() {
        //initiate the service
        val destinationService  = ServiceBuilder.buildService(OrdersService::class.java)
        val requestCall =destinationService.getAffectedOrdersList()
        //make network call asynchronously
        requestCall.enqueue(object : Callback<List<Orders>> {
            override fun onResponse(call: Call<List<Orders>>, response: Response<List<Orders>>) {
                Log.d("Data", "${response}")
                Log.d("Response", "onResponse With DATA: ${response.body()}")
                if (response.isSuccessful) {
                    val ordersList = response.body()!!
                    Log.d("Response", "orders list size : ${ordersList.size}")
                    var ord = findViewById<RecyclerView>(R.id.orders_recycler)
                    ord.apply {
                        layoutManager = GridLayoutManager(this@MainActivity, 2)
                        adapter = OrdersAdapter(response.body()!!)
                    }
                } else {
                    Toast.makeText(
                        this@MainActivity,
                        "Something went wrong ${response.message()}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onFailure(call: Call<List<Orders>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Something went wrong $t", Toast.LENGTH_SHORT)
                    .show()
            }
            /*
                }*/
            /*
        override fun onResponse(call: Call<OrdersList>, response: Response<OrdersList>) {
                response.body().forEach { order ->
                    Log.d("Data", "${response}")
                    Log.d("Response", "onResponse With DATA: ${response.body()}")
                    if (response.isSuccessful) {
                        val ordersList = order.body()!!
                        Log.d("Response", "orders list size : ${ordersList.size}")
                        var ord = findViewById<RecyclerView>(R.id.orders_recycler)
                        ord.apply {
                            layoutManager = GridLayoutManager(this@MainActivity, 2)
                            adapter = OrdersAdapter(order.body()!!)
                        }
                    } else {
                        Toast.makeText(
                            this@MainActivity,
                            "Something went wrong ${response.message()}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
               override fun onFailure(call: Call<OrdersList>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Something went wrong $t", Toast.LENGTH_SHORT).show()
            }
         */
        })
    }
}