package com.example.finalydaysvignaux.models

data class Command(
    val dueDate: String,
    val meals: List<Meal>,
    val price: Double
)